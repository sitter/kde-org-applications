#!/bin/bash

set -ex

[ -d node_modules ] || npm install browserify spdx-to-html

node_modules/browserify/bin/cmd.js spdx-to-html-app.js > spdx-to-html.js
