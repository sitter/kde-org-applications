#!/usr/bin/python
# -*- coding: utf-8 -*-
#############################################################################
# Copyright (C) 2009 Daniel Laidig <d.laidig@gmx.de>
#
# This script is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; see the file COPYING.LIB.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.
#############################################################################

import subprocess
from pipes import quote

class ScmDownloader:
    svnBase = "svn://anonsvn.kde.org/home/kde/"

    def __init__(self):
        pass

    def svnList(self, path):
        process = subprocess.Popen(["svn", "ls", self.svnBase + path], stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=None)
        output = process.communicate()
        result = process.returncode
        if result != 0:
            raise Exception(output[1])
        return output[0].split("\n")[:-1]

    def svnDownload(self, path, output):
        process = subprocess.Popen(["svn", "cat", self.svnBase + path], stdout=output, stderr=subprocess.PIPE, stdin=None)
        output = process.communicate()
        result = process.returncode
        if result != 0:
            raise Exception(output[1])
