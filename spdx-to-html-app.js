// Copyright 2018 Harald Sitter <sitter@kde.org>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by // the Free Software Foundation, either version 3 or any later version
// accepted by the membership of KDE e.V. (or its successor approved
// by the membership of KDE e.V.), which shall act as a proxy
// defined in Section 14 of version 3 of the license.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

var m_spdxToHTML = require('spdx-to-html')
// browserify will not expose the require directly, we'll wrap it into a
// global function instead
window.spdxToHTML = function(arg) {
    return m_spdxToHTML(arg)
}
