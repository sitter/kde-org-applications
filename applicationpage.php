<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

include 'includes/class_appdata2.inc';
$app = new AppData2($_GET['application']);
$category = $_GET['category'];
$development = isset($_GET['development']);

$page_title = $app->name();
if (!empty($app->genericName())) {
    $page_title = $page_title." - ".$app->genericName();
}

if ($development) {
    $page_title=$app->name()." - Development Information";
}
$page_title_extra_html = '<div class="app-icon"><img src="/applications/icons/'.$app->icon().'" alt="'.$app->name().' Icon" /></div>';

function printSidebar($app, $category, $backToOverview=false)
{
    $content = '';

    $content .= '<div id="infobox-return">';
    if ($backToOverview) {
        $content .= '<a href="/applications/'.$category.'/'.$app->id().'">Back to overview</a>';
    } else {
        $content .= <<<BACK_LINK
        <a href="/applications/$category">
            <img width="22" height="22" id="infobox-overviewicon" src="/applications/icons/categories/$category.svg" alt="" />
            Back to $category
        </a>
BACK_LINK;
    }
    $content .= '</div>';

    if ($app->hasHomepage() || $app->hasKDEApps()) {
        $content .= '<div class="infobox"><strong>More about '.$app->name().'</strong>';
        if ($app->hasHomepage()) {
            $content .= '<br /><a href="'.$app->homepage().'">'.$app->name().' Homepage</a>';
        }
        if ($app->hasKDEApps()) {
            $url = htmlspecialchars("http://kde-apps.org/content/show.php?content=".$app->KDEAppsId());
            $content .= '<br /><a href="'.$url.'">'.$app->name().' on KDE-Apps.org</a>';
        }
        $content .= '</div>';
    }

    $content .= '<div class="infobox"><strong>Get help</strong>';
    if ($app->hasUserbase()) {
        $content .= '<br /><a href="'.$app->userbase().'">'.$app->name().' on UserBase</a>';
    }
    $content .= '<br /><a href="'.$app->forumUrl().'">KDE Community Forums</a>';
    if ($app->hasHandbook()) {
        $content .= '<br /><a href="'.$app->handbook().'">'.$app->name().' Handbook</a>';
    }
    $content .= '</div>';

    $content .= '<div class="infobox"><strong>Contact the authors</strong>';

    //Bug tracker links
    if ($app->hasBugTracker()) {
        if ($app->isBugTrackerExternal()) {
            $content .= '<br /><a href="'.htmlentities($app->bugzillaProduct()).'">Report a bug</a>';
        } else { //KDE Bugzilla
            $componentstring = "";
            if ($app->bugzillaComponent()) {
                $componentstring = '&amp;component='.$app->bugzillaComponent();
            }
            $content .= '<br /><a href="https://bugs.kde.org/enter_bug.cgi?format=guided&amp;product='.$app->bugzillaProduct().$componentstring.'">Report a bug</a>';
        }
    } else { //Empty bugtracker, use default link
        $content .= '<br /><a href="https://bugs.kde.org/wizard.cgi">Report a bug</a>';
    }
    foreach ($app->ircChannels() as $channel) {
        $content .= '<br />IRC: <a href="irc://irc.freenode.org/'.$channel.'">'.$channel.' on Freenode</a>';
    }
    foreach ($app->mailingLists() as $ml) {
        //KDE mailing lists
        if (substr($ml, -8, 8) == "@kde.org") {
            $base = substr($ml, 0, -8);
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($ml).'">' .htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="https://mail.kde.org/mailman/listinfo/'.$base.'">subscribe</a>, <a href="https://mail.kde.org/mailman/listinfo/'.$base.'/">list information</a>)';
        } else if (substr($ml, -22, 22) == "@lists.sourceforge.net") { //Sourceforge.net
            $base = substr($ml, 0, -22);
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($ml).'">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="https://lists.sourceforge.net/lists/listinfo/'.$base.'">subscribe</a>, <a href="http://sourceforge.net/mailarchive/forum.php?forum_name='.$base.'">archive</a>)';
        } else if (substr($ml, 0, 31) == "http://groups.google.com/group/") { //Google Groups (web)
            $base = substr($ml, 31, strlen($ml)-31);
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($base).'@googlegroups.com">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="http://groups.google.com/group/'.$base.'/subscribe?note=1">subscribe</a>, <a href="http://groups.google.com/group/'.$base.'/topics">archive</a>)';
        } else if (substr($ml, -17, 17) == "@googlegroups.com") { //Google Groups (mail)
            $base = substr($ml, 0, -17);
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($ml).'">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="http://groups.google.com/group/'.$base.'/subscribe?note=1">subscribe</a>, <a href="http://groups.google.com/group/'.$base.'/topics">archive</a>)';
        } else { //Default mail
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($ml).'">'.$app->name().'</a>';
        }
    }
    $content .= '</div>';

    if (!$backToOverview) {
        $content .= '<div class="infobox"><strong><a href="'.$_SERVER['REQUEST_URI'].'/development">Development Information</a></strong></div>';
    }

    $content .= <<<EOT
        <link href="/applications/fonts/css/fontawesome-all.min.css" rel="stylesheet">
        <link rel="stylesheet" href="/applications/download.css">

        <div class="infobox"><strong>Get {$app->name()}</strong>
            <div class="downloadButton">
                <a href="appstream://{$app->AppStreamId()}">
                    <span class="fa fa-download"></span>
                    Install
                </a>
            </div>
        </div>
EOT;

    return $content;
}

function printPage($app)
{
    print '<div class="main-content">';

    //Print screenshot or dummy "no screenshot available" image

    $thumbUrl = $app->defaultScreenshotThumbnailUrl();
    $screenshotUrl = $app->defaultScreenshotUrl();
    if ($screenshotUrl) {
        print '<div class="app-screenshot"><a href="'.$screenshotUrl.'">
        <img src="'.$thumbUrl.'" width=540 alt="Screenshot" />
        </a></div>'; ///TODO: image size
    } else {
        print '<div class="app-screenshot">
        <img src="/images/screenshots/no_screenshot_available.png" alt="No screenshot available" />
        </div>'; ///TODO: image size
    }

    print $app->descriptionHtml();
    if ($app->hasVersions() || $app->hasAuthors() || $app->hasLicense()) {
        print '<p id="app-additional-info-paragraph"></p>';
        print '<div id="app-additional-info">';
        if ($app->hasAuthors()) {
            print '<h2>Developed By</h2>';
            print $app->authorHtml();
        }
        if ($app->hasVersions()) {
            ///TODO: Versions not yet implemented
        }
        if ($app->hasLicense()) {
            print '<h2>License</h2>';
            print $app->licenseHtml();
        }
        print '</div>';
    }

    //If JS available, hide the application details (authors) and show a toggle link
    ?>
    <script type="text/javascript">
    /* <![CDATA[ */
        $(document).ready(function(){
            $('#app-additional-info').toggle();
            $('#app-additional-info-paragraph').append('<a href="#" id="app-additional-info-toggle">Show more information about <?php print $app->name(); ?></a>');
            //Add toggle effect
            $('#app-additional-info-toggle').click(function(){
               $('#app-additional-info').slideToggle('slow');
               return false;
            });
        });
    /* ]]> */
    </script>
    <?php
    print '<div style="clear:left;"></div>';
    print '</div>';
}

function printDevelopmentPage($app)
{
    print '<div class="main-content">';

    print '<h2>Source code repository</h2>';

    print $app->browseSourcesHtml();

    print $app->checkoutSourcesHtml();

    //Show bugzilla related links only for applications hosted at bugs.kde.org
    if ($app->hasBugTracker() && !$app->isBugTrackerExternal()) {
        print '<h2>Search for open bugs</h2>';

        $product = $app->bugzillaProduct();

        $componentstring = "";
        if ($app->bugzillaComponent()) {
            $componentstring = '&component='.$app->bugzillaComponent();
        }

        $majorBugs  = 'https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash';
        $minorBugs  = 'https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=normal&bug_severity=minor';
        $wishes     = 'https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist';
        $juniorJobs = 'https://bugs.kde.org/buglist.cgi?keywords=junior-jobs&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&cmdtype=doit';

        print '<ul>';
        print '<li><a href="'.htmlspecialchars($majorBugs).'">Major Bug reports</a></li>';
        print '<li><a href="'.htmlspecialchars($minorBugs).'">Minor Bug reports</a></li>';
        print '<li><a href="'.htmlspecialchars($wishes).'">Wish reports</a></li>';
        print '<li><a href="'.htmlspecialchars($juniorJobs).'">Junior Jobs</a></li>';
        print '</ul>';
    }

    if ($app->hasEbn()) {
        print '<h2>Code checking</h2>';
        print '<p>Show results of automated code checking on the English Breakfast Network (EBN).</p>';
        print '<ul>';
        print '<li><a href="'.htmlspecialchars($app->ebnCodeCheckingUrl()).'">Code Checking</a></li>';
        print '<li><a href="'.htmlspecialchars($app->ebnDocCheckingUrl()).'">Documentation Sanitation</a></li>';
        print '</ul>';
    }

    print '</div>';
}

$sidebar_content = printSidebar($app, $category, $development);

//     error_reporting(E_ALL);
//     ini_set('display_errors', 1);
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => $page_title,
        'cssFile' => '/css/applications.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";


echo '<script src="/applications/spdx-to-html.js"></script>';

echo '<main class="container">';

if (!$development) {
    printPage($app);
} else {
    printDevelopmentPage($app);
}

echo '</main>';
require('../aether/footer.php');
