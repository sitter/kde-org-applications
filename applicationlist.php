<?php

include 'includes/class_appdata2.inc';

$index = json_decode(file_get_contents("index.json"), true);

$categories = array_keys($index);
$category = false;
foreach($categories as $cat) {
    if (strtolower($cat) == $_GET["category"]) {
        $category = $cat;
        break;
    }
}
if($category === false) {
    include("404.php");
    return;
}

require('../aether/config.php');

$page_title=$category;
$pageConfig = array_merge($pageConfig, [
     'title' => $page_title,
     'cssFile' => '/css/applications.css'
]);

require('../aether/header.php');
$site_root = "../";

echo '<main class="container">';

echo '<p><a href="/applications/">Back to the application index</a></p>';

foreach($index[$category] as $application) {
    $app = new AppData2($application);

// '<div class="app-icon"><img src="/images/icons/'.$app->icon().'" alt="'.$app->name().' Icon" /></div>';
    echo "<p class=\"app-category\">

      <a href=\"/applications/".strtolower($category)."/$application\">
        <img width=\"48\" height=\"48\" src=\"/applications/icons/".$app->icon()."\" alt=\"".$app->name()."\" title=\"".$app->name()."\" />
          ".$app->name()."
      </a>
      <br />
      ".$app->genericName()."</p>\n";
}

echo '<p>&nbsp;</p>';
echo '</main>';
require('../aether/footer.php');
